package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/gorilla/mux"
)

/*
	Rudimentary workaround for slow IPNS publishing issue. Essentially just a hosted hash table to associate thread
	names and thread IPFS file hashes. This will be running on a Google Compute Engine instance at 104.196.53.80:4001
 */

var cacheLocation string = "rockdove_thread_cache.json"
var threadCache map[string]string = make(map[string]string)

func main() {
	// Read cache file, if it exists
	cacheData, err := ioutil.ReadFile(cacheLocation)
	if err != nil {
		//log.Fatal(err)
		// TODO: Handle this properly (init empty file perhaps) before moving on.
		// Not a big deal as the program will write its cache on exit anyways
	} else {
		// Parse cache file
		err = json.Unmarshal(cacheData, &threadCache)
		if err != nil {
			log.Fatal(err)
		}
	}

	// Set up deferred function to write cache to file prior to closing
	defer func() {
		log.Println("Shutting Down - Writing Cache to Disk")


	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func(){
		<-c
		log.Println("Caught Forced Shutdown - Writing Thread Cache to Disk")
		writeCache()
		os.Exit(1)
	}()

	log.Println("Starting Server")

	// Set up router and listener
	router := mux.NewRouter()
	router.HandleFunc("/thread/{name}", GetIPFSId).Methods("GET")
	router.HandleFunc("/thread/{name}", RegisterThread).Methods("POST")
	log.Fatal(http.ListenAndServe(":4001", router))
}

func GetIPFSId(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	log.Println("Got Request for Thread: ", params["name"])

	matchingThreadHash := threadCache[params["name"]]

	w.Write([]byte(matchingThreadHash))
}

func RegisterThread(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	threadHashBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		// TODO: Return actual error response to requestor
		log.Fatal(err)
	}
	threadHash := string(threadHashBytes)

	log.Println("Caching Thread ", params["name"], " With Hash ", threadHash)

	threadCache[params["name"]] = threadHash
}

func writeCache() {
	cacheJSON, err := json.Marshal(threadCache)
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile(cacheLocation, cacheJSON, 0644)
	if err != nil {
		log.Fatal(err)
	}
}