
sources := $(wildcard *.go)

all: tags check

check:
	go test

runmain: main.go
	go run main.go

tags: $(sources)
	ctags -R $^

.PHONY: check all runmain
