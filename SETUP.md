# Development Setup Notes

## Install Steps
1. git
2. golang
3. Ensure GOROOT (Should be handled by installer) and GOPATH (Needs to be manually configured) are set up
	https://golang.org/doc/code.html#GOPATH
4. Ensure $GOPATH/bin has been added to $PATH
5. `go get -u -d github.com/ipfs/go-ipfs`
	Clones go-ipfs repo to $GOPATH/src/go-ipfs
6. Configure gx and gx-go (May only be necessary on Windows)
	6.1. Clone gx and gx-go
		`go get -u github.com/whyrusleeping/gx`
		`go get -u github.com/whyrusleeping/gx-go`
	6.2. Restart command line environment
	6.3. Instruct gx to install all IPFS-hosted go-ipfs dependencies
		`cd %GOPATH%/src/github.com/ipfs/go-ipfs`
		`gx --verbose install --global`
7. Clone Rockdove
	`go get -u gitlab.com/ksu-senior-project/rockdove`
	Enter gitlab credentials (or use SSH and keep your sanity)

## Execute Rockdove (Only contains IPFS testing code at the moment)
`cd $GOPATH/src/`

## Some Gotchas I Ran Into
* Had to forward port 4001 on router to read back files created by a prior IPFSNode instance
	* ipfs cat blocks forever (no timeout or extremely forgiving timeout?), but is immediately responsive after forwarding
* Windows 10 Creators Edition covertly remaps User/Documents folder to User/OneDrive/Documents in folder selection UI
	* Screwed up GOPATH environment variable. Had OneDrive go nuts trying to sync all dependent libraries to their cloud during install