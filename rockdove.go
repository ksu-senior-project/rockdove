package main

import (
	"gitlab.com/ksu-senior-project/rockdove/cmd"
	"fmt"
	"os"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// Set up initial IPFS connection
	/*_, err := rockdove.ConnectToIpfs(context.Background())
	if err != nil {
		log.Fatal(err)
	}*/

	/*thread, err := rockdove.OpenThread("TEST THREAD 2")
	if err != nil {
		log.Fatal(err)
	}

	for _, msg := range thread.MessageHistory(10) {
		fmt.Println("Posted Message: ", msg.Content)
	}*/

	// Create test thread
	/*thread, err := rockdove.NewThread("TEST THREAD 2")
	if err != nil {
		log.Fatal(err)
	}

	log.Println("THREAD PATH: ", thread.GetPath(), thread.Name)

	for i := 0; i < 10; i++ {
		// Create a test message
		msg := rockdove.NewMessage(fmt.Sprintf("TEST2_MESSAGE_%d", i))
		if err != nil {
			log.Fatal(err)
		}

		err = thread.Send(msg)
		if err != nil {
			log.Fatal(err)
		}
	}

	for _, msg := range thread.MessageHistory(10) {
		fmt.Println("Posted Message: ", msg.Content)
	}*/

	//fmt.Println("Sleeping to ensure IPFS node stays up")
	//time.Sleep(5 * time.Minute)
}
