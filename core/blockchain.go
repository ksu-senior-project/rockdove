package rockdove

import (
	"bytes"
	"context"
	"encoding/json"
	ipfsCore "github.com/ipfs/go-ipfs/core"
	"github.com/ipfs/go-ipfs/core/coreapi"
	"github.com/ipfs/go-ipfs/repo/fsrepo"
	ipfsApi "github.com/ipfs/go-ipfs/core/coreapi/interface"
	"fmt"
	"log"
)

type IpfsBlockchainI interface {
	GetPath() string
	SetPath(path string)

	getIPNSEntry() ipfsApi.IpnsEntry
	setIPNSEntry(ipfsApi.IpnsEntry)

	GetIPNSName() string
	SetIPNSName(ipnsName string)

	GetPrevious() IpfsBlockchainI
	SetPrevious(ipfsBlockChain IpfsBlockchainI)

	RetrievePreviousChain(length int) []IpfsBlockchainI
}

type IpfsBlockchain struct {
	previous  IpfsBlockchainI
	ipnsEntry ipfsApi.IpnsEntry

	Path     string
	PreviousHash string
	IPNSName string
}

func (bc *IpfsBlockchain) GetPath() string {
	return bc.Path
}

func (bc *IpfsBlockchain) SetPath(path string) {
	bc.Path = path
}

func (bc *IpfsBlockchain) GetIPNSName() string {
	return bc.IPNSName
}

func (bc *IpfsBlockchain) SetIPNSName(ipnsName string) {
	bc.IPNSName = ipnsName
}

// TODO: Better error handling here. Not very graceful at the moment
func (bc *IpfsBlockchain) GetPrevious() IpfsBlockchainI {
	if bc.previous == nil && bc.PreviousHash != "" {
		ipfsPath, err := coreapi.ParsePath(bc.PreviousHash)
		if err != nil {
			log.Fatal("Invalid previous hash")
		}

		bc.previous = &IpfsBlockchain{Path: ipfsPath.String()}

		err = Retrieve(ipfsPath, bc.previous)
		if err != nil {
			log.Fatal("Failed to retrieve previous entry from IPFS")
		}
	}
	return bc.previous
}

func (bc *IpfsBlockchain) SetPrevious(ipfsBlockChain IpfsBlockchainI) {
	bc.previous = ipfsBlockChain
	bc.PreviousHash = ipfsBlockChain.GetPath()
}

func (bc *IpfsBlockchain) RetrievePreviousChain(length int) []IpfsBlockchainI {
	previousBlocks := []IpfsBlockchainI{}

	var previousNode IpfsBlockchainI = bc.GetPrevious()
	i := 0
	for previousNode != nil && i < length {
		previousBlocks = append([]IpfsBlockchainI{previousNode}, previousBlocks...)
		previousNode = previousNode.GetPrevious()
		i++
	}

	return previousBlocks
}

func (bc *IpfsBlockchain) getIPNSEntry() ipfsApi.IpnsEntry {
	return bc.ipnsEntry
}

func (bc *IpfsBlockchain) setIPNSEntry(ipnsEntry ipfsApi.IpnsEntry) {
	bc.IPNSName = ipnsEntry.Name()
	bc.ipnsEntry = ipnsEntry
}

var ipfsNode *ipfsCore.IpfsNode
var ipfs ipfsApi.CoreAPI

// Really dumb thing for testing.
// TODO: Clean up default repo initialization
// TODO: Ensure any functions dependent on IPFS connection error properly if called before connection established
func ConnectToIpfs(ctx context.Context) (*ipfsCore.IpfsNode, error) {
	// Returned cached connection if available
	if ipfsNode != nil {
		return ipfsNode, nil
	}

	fmt.Println("Initializing IPFS")

	cfg := &ipfsCore.BuildCfg{}
	// Create configuration for IPFS node
	cfg.Online = true
	cfg.ExtraOpts = map[string]bool{
		"ipnsps": true,
		"pubsub": false,
	}

	// Open up a local ipfs if it exists
	r, err := fsrepo.Open("~/.ipfs")
	if err == nil {
		fmt.Println("Opening your local IPFS installation...")
		cfg.Repo = r
	} else {
		fmt.Println(err)
		fmt.Println("Opening a new IPFS installation...")
	}

	// Instantiate a new IPFS node
	ipfsNode, err := ipfsCore.NewNode(ctx, cfg)
	fmt.Println("BOUND TO ", len(ipfsNode.PeerHost.Addrs()), " ADDRESSES")
	for _, addr := range ipfsNode.PeerHost.Addrs() {
		fmt.Println("ADDR: ", addr.String())
	}

	log.Println("HAS ", len(ipfsNode.Peerstore.Peers()), " PEERS")
	for _, peer := range ipfsNode.Peerstore.Peers() {
		fmt.Println("PEER: ", peer.String())
	}

	fmt.Println("Is Online Mode: ", ipfsNode.OnlineMode())

	// Retrieve the Object and Name core api wrappers
	ipfs = coreapi.NewCoreAPI(ipfsNode)

	return ipfsNode, err
}

func Store(ipfsBlockchain IpfsBlockchainI) error {
	ctx := context.Background()

	// Serialize current instance as JSON
	asJSON, err := json.Marshal(ipfsBlockchain)
	if err != nil {
		return err
	}
	asJSONBuffer := bytes.NewBuffer(asJSON)

	// Save to IPFS
	ipfsPath, err := ipfs.Unixfs().Add(ctx, asJSONBuffer)
	if err != nil {
		return err
	}

	//fmt.Println("Stored ", string(asJSON), " as ", ipfsPath)

	// Ensure the stored path is up to date
	ipfsBlockchain.SetPath(ipfsPath.String())

	return nil
}

func StoreAndPublish(ipfsBlockchain IpfsBlockchainI) error {
	ctx := context.Background()

	// Store to IPFS
	err := Store(ipfsBlockchain)
	if err != nil {
		return err
	}

	// Publish to IPNS
	log.Println("Waiting on IPNS publish")
	asPath, err := coreapi.ParsePath(ipfsBlockchain.GetPath())
	if err != nil {
		return err
	}

	ipnsEntry, err := ipfs.Name().Publish(ctx, asPath)
	if err != nil {
		return err
	}
	log.Println("IPNS publish complete")

	// Ensure the stored IPNS name is up to date
	ipfsBlockchain.setIPNSEntry(ipnsEntry)

	return nil
}

func StoreAndPublishWithName(name string, ipfsBlockchain IpfsBlockchainI) error {
	err := Store(ipfsBlockchain)
	if err != nil {
		return err
	}

	err = PublishHash(name, ipfsBlockchain.GetPath())
	if err != nil {
		return err
	}

	return nil
}

func Retrieve(path ipfsApi.Path, ipfsBlockchain IpfsBlockchainI) error {
	ctx := context.Background()

	// Read content from IPFS
	//fmt.Println("Getting file contents")
	reader, err := ipfs.Unixfs().Cat(ctx, path)
	if err != nil {
		return err
	}
	//fmt.Println("Got file contents")

	// Construct IpfsBlockchain instance
	err = json.NewDecoder(reader).Decode(ipfsBlockchain)
	if err != nil {
		return err
	}

	// Ensure stored path is up to date
	ipfsBlockchain.SetPath(path.String())

	return nil
}

func RetrievePublished(ipnsName string, ipfsBlockchain IpfsBlockchainI) error {
	ctx := context.Background()

	// Retrieve IPFS path from published IPNS name
	log.Println("Waiting on IPNS resolve")
	ipfsPath, err := ipfs.Name().Resolve(ctx, ipnsName, nil)
	if err != nil {
		return err
	}
	log.Println("IPNS resolve complete")

	// Retrieve content at IPFS path
	err = Retrieve(ipfsPath, ipfsBlockchain)
	if err != nil {
		return err
	}

	// Ensure stored IPNS name is up to date
	ipfsBlockchain.SetIPNSName(ipnsName)

	return nil
}

func RetrievePublishedWithName(name string, ipfsBlockchain IpfsBlockchainI) error {
	ipfsHash, err := ResolveHash(name)
	if err != nil {
		return err
	}

	//fmt.Println("Resolved hash: ", ipfsHash)

	ipfsPath, err := coreapi.ParsePath(ipfsHash)
	if err != nil {
		return err
	}

	err = Retrieve(ipfsPath, ipfsBlockchain)
	if err != nil {
		return err
	}
	//fmt.Println("Retrieved contents")

	return nil
}

// TODO: May be better to take this approach to separate long running network retrieval from basic getter behavior
/*
func (bc *IpfsBlockchain) FindPrevious() *IpfsBlockchain {
	if bc.previous != nil {
		return bc.previous
	}

	// Look up file
	return nil
}
*/

/*func (bc *IpfsBlockchain) MarshalJSON() ([]byte, error) {
	if bc.previous != nil {
		return []byte(fmt.Sprintf(`"prev": "%v",`, bc.previous.Path)), nil
	}
	return nil, nil
}*/
