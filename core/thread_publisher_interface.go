package rockdove

import (
	"io/ioutil"
	"net/http"
	"strings"
)

var threadPublisherAddress string = "http://104.196.53.80:4001"

func PublishHash(name string, hash string) error {
	hashReader := strings.NewReader(hash)
	_, err := http.Post(threadPublisherAddress + "/thread/" + name, "text/plain; charset=utf-8", hashReader)
	return err
}

func ResolveHash(name string) (string, error) {
	resolveResponse, err := http.Get(threadPublisherAddress + "/thread/" + name)
	if err != nil {
		return "", err
	}

	threadHash, err := ioutil.ReadAll(resolveResponse.Body)
	resolveResponse.Body.Close()
	if err != nil {
		return "", err
	}

	return string(threadHash), nil
}