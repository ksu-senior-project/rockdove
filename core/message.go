package rockdove

import (
	"github.com/ipfs/go-ipfs/core/coreapi"
	"log"
	"time"
)

type Message struct {
	*IpfsBlockchain // Support adding to blockchain
	User string
	Content         string
	IsSaved bool
	TimePosted time.Time
}

func NewMessage(args ...string) *Message {
	return &Message{&IpfsBlockchain{}, args[1], args[0], false, time.Now()}
}

func (m *Message) String() string {
	return m.Content
}

// TODO: Fix the object mode. Gotta be a way to make this work without re-defining the same behavior, but pointing to a different struct
func (msg *Message) GetPrevious() IpfsBlockchainI {
	if msg.previous == nil && msg.PreviousHash != "" {
		ipfsPath, err := coreapi.ParsePath(msg.PreviousHash)
		if err != nil {
			log.Fatal("Invalid previous hash")
		}

		msg.previous = &Message{&IpfsBlockchain{Path: ipfsPath.String()}, "", "",false, time.Now()}

		err = Retrieve(ipfsPath, msg.previous)
		if err != nil {
			log.Fatal("Failed to retrieve previous entry from IPFS")
		}
	}
	return msg.previous
}

func (msg *Message) SetPrevious(ipfsBlockChain IpfsBlockchainI) {
	msg.previous = ipfsBlockChain
	msg.PreviousHash = ipfsBlockChain.GetPath()
}

func (msg *Message) RetrievePreviousChain(length int) []IpfsBlockchainI {
	previousBlocks := []IpfsBlockchainI{}

	var previousNode IpfsBlockchainI = msg.GetPrevious()
	i := 0
	for previousNode != nil && i < length {
		previousBlocks = append([]IpfsBlockchainI{previousNode}, previousBlocks...)
		previousNode = previousNode.GetPrevious()
		i++
	}

	return previousBlocks
}