package rockdove

import (
	"errors"
	"time"

	"github.com/ipfs/go-ipfs/core/coreapi"
)

// Thread is a manipulatable series of messages.
type Thread struct {
	*IpfsBlockchain

	//entry        ipfsApi.IpnsEntry // Entry for this thread
	//path         ipfsApi.Path

	MessageHead  string
	message_head *Message
	Name         string
}

// Create a new thread.
func NewThread(name string) (*Thread, error) {
	thr := &Thread{IpfsBlockchain: &IpfsBlockchain{}, Name: name}
	err := StoreAndPublishWithName(name, thr)
	return thr, err
}

func OpenThread(name string) (*Thread, error) {
	data := &Thread{IpfsBlockchain: &IpfsBlockchain{}, Name: name}
	err := RetrievePublishedWithName(name, data)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (t *Thread) Send(msg *Message) error {
	if msg.IsSaved {
		return errors.New("Message is already saved.")
	}

	// Bind the new message to the previous head message
	if t.message_head != nil {
		msg.SetPrevious(t.message_head)
	}

	msg.TimePosted = time.Now()

	// Store the new message
	err := Store(msg)
	if err != nil {
		return err
	}

	// Set the thread's head message to this new message
	t.setMessageHead(msg)

	// Publish the updated thread
	err = StoreAndPublishWithName(t.Name, t)
	if err != nil {
		return err
	}

	return nil
}

func (t *Thread) setMessageHead(msg *Message) {
	t.message_head = msg
	//fmt.Println("Setting message head to: ", msg.GetPath())
	t.MessageHead = msg.GetPath()
}

//
func (t *Thread) String() string {
	return "<Thread>"
}

func (t *Thread) MessageHistory(length int) (messages []*Message) {
	messages = []*Message{}

	// TODO: Clean this up
	if t.MessageHead != "" && t.message_head == nil {
		ipfsPath, err := coreapi.ParsePath(t.MessageHead)
		if err == nil {
			msg := &Message{}
			err = Retrieve(ipfsPath, msg)

			if err == nil {
				t.message_head = msg
			}
		}
	}

	if t.message_head != nil {
		previousBlocks := t.message_head.RetrievePreviousChain(length)
		for _, previousBlock := range previousBlocks {
			// TODO: There's no way I'm doing this right.
			messages = append(messages, previousBlock.(*Message))
		}

		messages = append(messages, t.message_head)
	}

	return
}
