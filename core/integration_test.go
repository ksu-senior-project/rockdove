package rockdove_test

// Integration tests for Rockdove are here.

import (
	"context"
	"fmt"
	ipfsCore "github.com/ipfs/go-ipfs/core"
	coreapi "github.com/ipfs/go-ipfs/core/coreapi"
	"testing"
	"time"
)

func makeTodoConnection(t *testing.T) (*ipfsCore.IpfsNode, context.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Hour*24)

	node, err := ConnectToIpfs(ctx)
	if err != nil {
		cancel()
		t.Fatal(err)
	}

	return node, ctx
}

func TestCreateMessage(t *testing.T) {
	msg := NewMessage("hello")
	t.Log(msg)
}

func TestCreateThreadType(t *testing.T) {
	node, _ := makeTodoConnection(t)
	ipfs := coreapi.NewCoreAPI(node)

	thr, err := NewThread(ipfs, "test")
	if err != nil {
		t.Fatal(err)
	}

	t.Log(thr)
	thr.Send(NewMessage("first"))
	fmt.Println(thr.String())
	if thr.String() != "<Thread>" {
		t.Fail()
	}
}

func TestConnectToIpfs(t *testing.T) {
	node, ctx := makeTodoConnection(t)
	t.Log(node, ctx)
}

func TestCreateBlockchain(t *testing.T) {
	bc := IpfsBlockchain{}
	t.Log(bc)
}
