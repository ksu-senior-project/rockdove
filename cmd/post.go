package cmd

import (
	"fmt"
	"net/http"
	"strings"
	"log"
	"github.com/spf13/cobra"
)

var PostCmd = &cobra.Command{
	Use: "post",
	Run: func(cmd *cobra.Command, args []string) {
		msgContent := args[0]
		fmt.Println("Posting message: ", msgContent)

		msgReader := strings.NewReader(msgContent)
		_, err := http.Post("http://localhost:4002/", "text/plain; charset=utf-8", msgReader)

		if err != nil {
			log.Fatal(err)
		}
	},
}
