package cmd

import (
	"context"
	"log"
	//"fmt"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/ksu-senior-project/rockdove/core"
)

var CreateThreadCmd = &cobra.Command{
	Use: "create",
	Run: func(cmd *cobra.Command, args []string) {
		threadName := args[0]
		//fmt.Println("Creating room: ", threadName)

		// Set up initial IPFS connection
		_, err := rockdove.ConnectToIpfs(context.Background())
		if err != nil {
			log.Fatal(err)
		}

		_, err = rockdove.NewThread(threadName)
		if err != nil {
			log.Fatal(err)
		}

		time.Sleep(5 * time.Second)

		//fmt.Println("Room Created Successfully")
	},
}