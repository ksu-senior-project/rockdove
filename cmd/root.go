package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(CreateThreadCmd)
	RootCmd.AddCommand(JoinCmd)
	RootCmd.AddCommand(PostCmd)
}

var RootCmd = &cobra.Command{
	Use: "rockdove",
	Short: "Rockdove is a distributed chat system built on IPFS",
	Long: `rockdove is the main command, used to interact with the system.

Try running one of the following commands to get started:
Join a Thread: "rockdove join <thread-name>"
Post a Message to a Thread: "rockdove post <thread-name> <message>"
`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(cmd.Long)
	},
}
