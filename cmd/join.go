package cmd

import (
	"context"
	"io/ioutil"
	"log"
	"fmt"
	"net/http"
	"time"
	"os"
	"os/exec"
	"runtime"

	"github.com/spf13/cobra"
	"gitlab.com/ksu-senior-project/rockdove/core"
)

var clear map[string]func() //create a map for storing clear funcs

func init() {
	clear = make(map[string]func()) //Initialize it
	clear["linux"] = func() {
		cmd := exec.Command("clear") //Linux example, its tested
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
	clear["windows"] = func() {
		cmd := exec.Command("cmd", "/c", "cls") //Windows example, its tested
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
	clear["darwin"] = func() {
		cmd := exec.Command("clear")
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
}

func callClear() {
	value, ok := clear[runtime.GOOS] //runtime.GOOS -> linux, windows, darwin etc.
	if ok { //if we defined a clear func for that platform:
		value()  //we execute it
	} else { //unsupported platform
		panic("Your platform is unsupported! I can't clear terminal screen :(")
	}
}

var JoinCmd = &cobra.Command{
	Use: "join",
	Run: func(cmd *cobra.Command, args []string) {
		threadName := args[0]

		userName := ""
		if len(os.Args) > 1 {
			userName = args[1]
		}

		fmt.Println("Joining room: ", threadName)

		// Set up initial IPFS connection
		_, err := rockdove.ConnectToIpfs(context.Background())
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("Rockdove Initialized")

		postChannel := make(chan string)

		// Start local server to catch posts
		go func() {
			http.HandleFunc("/", func (w http.ResponseWriter, r *http.Request) {
				//fmt.Println("Received post request")
				messageContentB, err := ioutil.ReadAll(r.Body)
				if err != nil {
					log.Fatal(err)
				}
				messageContent := string(messageContentB)
				postChannel <- messageContent
			});

			fmt.Println("Starting post listener")
			log.Fatal(http.ListenAndServe("localhost:4002", nil))
		}()

		// Start thread pinger to update thread state
		for {
			//fmt.Println("Opening Thread")
			thread, err := rockdove.OpenThread(threadName)
			if err != nil {
				log.Fatal(err)
			}

			//fmt.Println("Opened thread")

			callClear()

			fmt.Printf(`
============================================
========  Thread: %s  ==============
============================================
`, threadName)

			for _, msg := range thread.MessageHistory(20) {
				fmt.Printf("[%s] %s: %s\n\n", msg.User, msg.TimePosted.Format("2006-01-02 15:04:05"), msg.Content)
			}

			//fmt.Println("Got message history")

			// Non-blocking channel retrieval for any new messages to post to the thread
			//fmt.Println("Waiting on next post request")
			select {
				case messageContent := <-postChannel:
					msg := rockdove.NewMessage(messageContent, userName)
					if err != nil {
						log.Fatal(err)
					}

					err = thread.Send(msg)
					if err != nil {
						log.Fatal(err)
					}
				default:
					// No queued message immediately available, skip
			}

			//fmt.Println("Skipping post request")

			time.Sleep(2 * time.Second)
		}
	},
}
